import os

from django.conf.urls.defaults import *

from apps.blog.views import get_all_articles

handler500 = 'djangotoolbox.errorviews.server_error'

urlpatterns = patterns('',
    url(r'^accounts/', include('apps.accounts.urls')),
    url(r'^articles/', include('apps.blog.urls')),
    url('^$', get_all_articles, name='home'),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',\
         {'document_root': os.path.join(os.path.dirname(__file__), 'static')}),
)
