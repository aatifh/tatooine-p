from django import forms

import acc_dbapi

class SigninForm(forms.Form):
    """A model form for signin.
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, label='Password', required=True)
    re_password = forms.CharField(widget=forms.PasswordInput, label='Re-type Password', required=True)

    class Meta:
        model = acc_dbapi.User
        fields = ('username', 'email',)

    def clean_username(self):
        username = self.cleaned_data['username']
        if not username:
            raise forms.ValidationError('This field is required.')
        elif acc_dbapi.get_user(username=username):
            raise forms.ValidationError('This username is already taken.')
        return username

    def clean_email(self):
        email = self.cleaned_data['email']
        if not email:
            raise forms.ValidationError('This field is required.')
        elif acc_dbapi.get_user(email=email):
            raise forms.ValidationError('This email is already taken.')
        return email

    def clean_re_password(self):
        re_password = self.cleaned_data['re_password']
        password = self.cleaned_data.get('password')
        if not password:
            return re_password
        if re_password != password:
            raise forms.ValidationError('Password do not match.')
        return re_password

    def save(self, commit=False, **kwargs):
        obj = super(SignupForm, self).save(commit=commit, **kwargs)
        obj.set_password(self.cleaned_data['password'])
        obj.save()
        return obj

