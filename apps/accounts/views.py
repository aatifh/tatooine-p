import base64

from django.contrib import auth
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.sites.models import get_current_site
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.utils.http import urlquote, base36_to_int, int_to_base36
from django.contrib.auth.forms import PasswordResetForm,\
    SetPasswordForm

from django.contrib.auth.models import User

from forms import SigninForm, SignupForm
from apps.messages import RESPONSE_MESSAGES

def _validate_user(username, password):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return
    else:
        return auth.authenticate(username=user.username,\
                                     password=password)

def signin(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    context = RequestContext(request)
    form = SigninForm()
    next = request.GET.get('next')
    if next:
        request.session['next'] = next
    if request.method == 'POST':
        form = SigninForm(request.POST)
        if form.is_valid():
            user = _validate_user(form.cleaned_data['username'],\
                                      form.cleaned_data['password'])
            if not user:
                context['errors'] = 'Invalid username or password.'
            elif not user.is_active:
                context['errors'] = 'Your account has not been activated yet.'
            else:
                auth.login(request, user)
                next = request.session.get('next', '/')
                return HttpResponseRedirect(next)
    context['form'] = form
    return render_to_response('accounts/login.html',\
                                  context_instance=context)

def signup(request):
    context = RequestContext(request)
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            obj = form.save()
            context['message'] = RESPONSE_MESSAGES.get('registered-successfully')
            return render_to_response('response-message.html',\
                                          context_instance=context)

    else:
        form = SignupForm()

    context['form'] = form
    return render_to_response('accounts/signup.html',\
                                  context_instance=context)


@login_required
def signout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home'))

def forgot_password(request):
    pass

def reset_password(request, uidb36=None, token=None):
    pass
