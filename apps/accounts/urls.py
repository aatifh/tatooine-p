from django.conf.urls.defaults import *

urlpatterns = patterns('apps.accounts.views',
    url(r'^signup/?$', 'signup', name='signup'),
    url(r'^login/?$', 'signin', name='signin'),
    url(r'^logout/?$', 'signout', name='signout'),
    url(r'^forgot-password/?$', 'forgot_password', name='forgot-password'),
    url(r'^reset-password/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'reset_password', name='reset-password'),
)

