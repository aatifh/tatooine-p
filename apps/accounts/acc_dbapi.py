from django.contrib.auth.models import User

def get_user(*args, **kwargs):
    try:
        return User.objects.get(*args, **kwargs)
    except User.DoesNotExist:
        return
