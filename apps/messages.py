RESPONSE_MESSAGES = {'registered-successfully': 'Thanks for signing up! Please try to login now.',
                     'forgot-password': 'We have sent you an email with instructions to reset your password.',
                     'reset-password': 'Your password has been set. You may go ahead and login now.',}

