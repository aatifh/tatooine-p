from django.conf import settings
from django.template import Library

register = Library()

@register.filter
def get_counter(page_number, counter, page_index=settings.PAGINATION_LIMIT):
    return (page_number * page_index - page_index) + counter

