from django.conf import settings
from django.utils import simplejson as json
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect,\
    HttpResponseNotAllowed

import blog_dbapi
from forms import ArticleForm
from apps.utils import pagination
from apps.accounts import acc_dbapi
from apps.messages import RESPONSE_MESSAGES

def get_all_articles(request):
    """Get all public articles.
    """
    context = RequestContext(request)
    articles = blog_dbapi.get_all_articles(is_public=True)
    context['results'] = pagination(request, articles, settings.PAGINATION_LIMIT)
    context['sub_title'] = 'All articles'
    return render_to_response('blog/articles.html', context_instance=context)


@login_required
def get_user_articles(request, username=None):
    """Get all user submitted articles
    """
    context = RequestContext(request)
    if username:
        posted_by = acc_dbapi.get_user(username=username)
        if posted_by is None:
            raise Http404()
        articles = blog_dbapi.get_all_articles(created_by=posted_by, is_public=True)
        context['sub_title'] = "%s's articles" % posted_by.username
    else:
        articles = blog_dbapi.get_all_articles(created_by=request.user)
        context['sub_title'] = 'Your articles'
    context['results'] = pagination(request, articles, settings.PAGINATION_LIMIT)

    return render_to_response('blog/articles.html', context_instance=context)


@login_required
def add_article(request):
    context = RequestContext(request)
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            obj = form.save(request.user)
            return HttpResponseRedirect(reverse('article-detail', kwargs={'slug': obj.slug}))
    else:
        form = ArticleForm()
    context['form'] = form
    return render_to_response('blog/add-article.html',\
                                  context_instance=context)

@login_required
def edit_article(request, slug):
    article = blog_dbapi.get_article(slug=slug)
    if article is None or article.created_by != request.user:
        raise Http404()

    context = RequestContext(request)
    if request.method == 'POST':
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            obj = form.save(request.user)
            return HttpResponseRedirect(reverse('article-detail', kwargs={'slug': obj.slug}))
    else:
        form = ArticleForm(instance=article)
    context['form'] = form
    return render_to_response('blog/add-article.html',\
                                  context_instance=context)

def article_detail(request, slug):
    article = blog_dbapi.get_article(slug=slug)
    if article is None:
        raise Http404()
    context = RequestContext(request)
    context['article'] = article
    context['total_response'] = blog_dbapi.get_all_response(article=article)
    context['is_authenticated'] = request.user.is_authenticated()
    return render_to_response('blog/article-detail.html',\
                                  context_instance=context)

@login_required
def delete_article(request, slug):
    article = blog_dbapi.get_article(slug=slug)
    if article is None or article.created_by != request.user:
        raise Http404()
    context = RequestContext(request)
    blog_dbapi.delete_response(article)
    article.delete()
    return HttpResponseRedirect(reverse('get-articles'))


######################################
#   Article response related views   #
######################################

@login_required
@csrf_exempt
def respond_to_article(request, slug, in_reply_to=None):
    # INFO: It should only accept POST request!
    if request.method != 'POST':
        return HttpResponseNotAllowed('POST')

    article_obj = blog_dbapi.get_article(slug=slug)
    if article_obj is None:
        return HttpResponse(json.dumps({'success': False, 'message':\
                                            'Invalid Article slug!'}), mimetype='application/javascript')
    in_reply_to_obj = blog_dbapi.get_response(id=in_reply_to)\
        if in_reply_to else None
    if in_reply_to and in_reply_to_obj is None:
        return HttpResponse(json.dumps({'success': False, 'message':\
                                            'Invalid in_reply_to id!'}), mimetype='application/javascript')

    text = request.POST.get('text')
    response_obj = blog_dbapi.create_response(article=article_obj, text=text,\
                                                  created_by=request.user, in_reply_to=in_reply_to_obj)
    return HttpResponse(json.dumps({'success': True, 'message':\
                                        'Response posted successfully!'}), mimetype='application/javascript')

