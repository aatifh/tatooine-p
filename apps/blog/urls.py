from django.conf.urls.defaults import *

urlpatterns = patterns('apps.blog.views',
    url(r'^/?$', 'get_user_articles', name='get-articles'),
    url(r'^add/?$', 'add_article', name='add-article'),
    url(r'^user/(?P<username>.+)/?$', 'get_user_articles', name='get-user-articles'),
    url(r'^(?P<slug>.+)/edit/?$', 'edit_article', name='edit-article'),
    url(r'^(?P<slug>.+)/delete/?$', 'delete_article', name='delete-article'),
    url(r'^(?P<slug>.+)/respond/?$', 'respond_to_article', name='respond-to-article'),
    url(r'^(?P<slug>.+)/respond/(?P<in_reply_to>[0-9]+)/$', 'respond_to_article', name='respond-to-article'),
    url(r'^(?P<slug>.+)/$', 'article_detail', name='article-detail'),
)

