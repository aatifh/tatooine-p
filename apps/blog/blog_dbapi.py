from django.db.models import Q
from models import Article, Response

def get_all_articles(*args, **kwargs):
    return Article.objects.filter(*args, **kwargs).order_by('-created_at')

def get_article(*args, **kwargs):
    try:
        return Article.objects.get(*args, **kwargs)
    except Article.DoesNotExist:
        return

def get_response(*args, **kwargs):
    try:
        return Response.objects.get(*args, **kwargs)
    except Response.DoesNotExist:
        return

def create_response(*args, **kwargs):
    return Response.objects.create(*args, **kwargs)

def get_all_response(*args, **kwargs):
    return Response.objects.filter(*args, **kwargs)

def delete_response(article):
    return Response.objects.filter(article=article).delete()
