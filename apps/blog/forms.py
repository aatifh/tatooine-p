from django import forms

import blog_dbapi


class ArticleForm(forms.ModelForm):
    """
    """
    class Meta:
        model = blog_dbapi.Article
        fields = ('title', 'description', 'is_public', )

    def save(self, created_by=None, commit=True):
        obj = super(ArticleForm, self).save(commit=False)
        if created_by:
            obj.created_by = created_by
        obj.save()
        return obj
