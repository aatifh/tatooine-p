from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, Http404

from apps.unique_slugify import unique_slugify

class Article(models.Model):
    """A model to hold the article records
    """
    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=100, verbose_name=_('Title'))
    description = models.TextField(verbose_name=_('Description'))
    created_by = models.ForeignKey(User, related_name='article_created_by')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    is_public = models.BooleanField(verbose_name=_('Is Public?'), default=True)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        unique_slugify(self, self.title)
        super(Article, self).save(*args, **kwargs)

class Response(models.Model):
    """A model to hold responses for Article.
    """
    text = models.TextField(verbose_name=_('Response text'))
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    created_by = models.ForeignKey(User, related_name='response_created_by')
    article = models.ForeignKey(Article, related_name='article')
    in_reply_to = models.ForeignKey('self', null=True, blank=True, default=None)

    def replies(self):
        return Response.objects.filter(in_reply_to=self).order_by('created_at')

    def __unicode__(self):
        return '%s' % self.text[:8]
