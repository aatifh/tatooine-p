from apps.paginator import Paginator, EmptyPage,\
    PageNotAnInteger, InvalidPage

def pagination(request, results, result_per_page):
    paginator = Paginator(results, result_per_page)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        results = paginator.page(page)
    except (EmptyPage, InvalidPage):
        results = paginator.page(paginator.num_pages)

    return results
