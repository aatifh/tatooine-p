$(document).ready(function() {
  $("a[id^='delete-article']").click(function(e){
	id = this.id;
    var answer = confirm ("Are you sure you want to delete this article?")
    if (answer) {
        return true;
    } else
        return false;
  });

  $("a[id^='in-reply-to_']").click(function(e){
	id = this.id;
    var is_authenticated = $('input[name="is-authenticated"]').val();
    // console.log (typeof is_authenticated);
    // console.log (is_authenticated);
    if (is_authenticated.toLowerCase() === "false") {
      var article_slug = $('input[name="article-slug"]').val();
      window.location = '/accounts/login/?next=' + '/articles/' + article_slug;
    }
    var in_reply_to = id.split("_")[1];
    if (in_reply_to) {
      $('input[name="in-reply-to"]').val(in_reply_to);
      return true;
    } else
      return true;
  });

  $("#submit-response").click(function (e) {
    var text = $.trim($('#response-text').val());
    if (text === "") {
      $('.error-modal').show();
      return false;
    }
    var article_slug = $('input[name="article-slug"]').val();
    var in_reply_to = $('input[name="in-reply-to"]').val();

    if (in_reply_to) {
      url = "/articles/" + article_slug + "/respond/" + in_reply_to + "/";
    } else {
      url = "/articles/" + article_slug + "/respond/";
    }
    $.ajax({
      type: 'POST',
      url: url,
      data: {
        text: text,
      },
      dataType:'json',
      success: function(data) {
        if (data.success === true) {
            $('#myModal').hide ();
            alert(data.message);
            window.location = '/articles/' + article_slug;
            // $('#alertModal').removeClass('hide');
            // $('#alertModal').addClass('custom-show');
          // alert ($('span[id="server-message"]')).val ()
          //$('span[id="server-message"]').val(data.message);
        } else {
            $('.error-modal').text(data.message);
            $('.error-modal').show();
            return false;
        }
        return false;
      }
    });
  });
});
